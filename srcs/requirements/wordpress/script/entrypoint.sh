#!/bin/sh

if [ ! -f "/var/www/localhost/wp-config.php" ]; then

	echo "Installing wordpress";

	cp -R /tmp/wordpress/* /var/www/localhost/;

	wp-cli.phar config create \
	--dbname=$DB_NAME \
	--dbuser=$DB_USER \
	--dbpass=$DB_PASSWORD \
	--dbhost=$DB_HOST \
	--dbprefix=wp_ \
	--path=/var/www/localhost/ \
	--allow-root;

	wp-cli.phar core install \
	--url=$WP_URL \
	--title=$WP_TITLE \
	--admin_user=$WP_ADMIN_USER \
	--admin_email=$WP_ADMIN_EMAIL \
	--admin_password=$WP_ADMIN_PASSWORD \
	--path=/var/www/localhost/ \
	--allow-root ;

	wp-cli.phar user create $WP_USER $WP_EMAIL \
	--user_pass=$WP_PASSWORD \
	--path=/var/www/localhost/ \
	--role=author \
	--allow-root ;

	wp-cli.phar option set default_comment_status closed --allow-root --path=/var/www/localhost/;

	wp-cli.phar post create --post_type='post' --post_status=publish --post_title='Titre' --post_content="Contenu." --post_author=2 --path=/var/www/localhost/ --allow-root;

	chown -R www-data:www-data /var/www/localhost;
	chmod -R 755 /var/www/localhost;
fi

php-fpm7.4 -F;
