NAME = inception

all: $(NAME)

$(NAME): build
	@make up

build:
	@mkdir -p ${HOME}/data/wordpress
	@mkdir -p ${HOME}/data/mariadb
	@docker-compose -f ./srcs/docker-compose.yml build

up:
	@docker-compose -f ./srcs/docker-compose.yml up -d

down:
	@docker-compose -f ./srcs/docker-compose.yml down

re:
	@make down
	@make clean
	@make build
	@make up

clean: down
	@docker system prune -af
	@rm -rf $(HOME)/data
#	@ { docker volume ls -q; echo NULL;} | xargs -r docker volume rm --force
#	@sudo rm -rf ${HOME}/data/

.phony: all re down clean up build
